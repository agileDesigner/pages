# pages

This is my [static](#static) website hosted by Codeberg.org.

##### Status: Updated frequently

Check it out at: [https://agiledesigner.codeberg.page](https://agiledesigner.codeberg.page "Agile Designer | Homepage")

The plan for this website is that it is just my personal site, which I post content and play with code (mostly HTML and CSS) on.

### Currently the content I post falls into four categories:

<dl>
	<dt>Posts</dt>
	<dl>General things such as small projects and advice.</dl>
	<dt>Ideas</dt>
	<dl>Things I think would be a good idea.</dl>
	<dt>Projects</dt>
	<dl>Projects I'm working on/worked on.</dl>
	<dt>Tips</dt>
	<dl>Short posts consisting of small amounts of useful information.</dl>
</dl>

Enjoy!

# Glossary

#### Static

Static webpages are webpages which serve exactly the content stored on its server to a visitor. Therefore all visitors to the webpage will receive the same content when visiting the page. A static website is a website which only contains static webpages.
